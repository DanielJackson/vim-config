" Daniel's vimrc
" Install vim-plug if not found
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
endif

" Run PlugInstall if there are missing plugins
autocmd VimEnter * if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
  \| PlugInstall --sync | source $MYVIMRC
\| endif

" Specify a directory for plugins
" - For Vim: default is ~/.vim/plugged
" - For Neovim: stdpath('data') . '/plugged'
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.vim/plugged')

" Base 'sensible' Vim configuration
Plug 'tpope/vim-sensible'

" Git
Plug 'tpope/vim-fugitive'

" color schemes (I'm indecisive...)
Plug 'jnurmine/zenburn'
Plug 'cocopon/iceberg.vim'
Plug 'sainnhe/everforest'

" Code completion and intellisense
Plug 'neoclide/coc.nvim', {'branch': 'release'}

" Asynchronous Lint Engine
Plug 'dense-analysis/ale'

" NERD Tree
Plug 'scrooloose/nerdtree'

" super light status bar
Plug 'bling/vim-airline'

" git context on line-by-line basis in buffers
Plug 'airblade/vim-gitgutter'
Plug 'kien/rainbow_parentheses.vim'
Plug 'tpope/vim-surround'

" Rust stuff
Plug 'rust-lang/rust.vim' 

" Initialize plugin system
call plug#end()

" Put your non-Plugin stuff after this line
set encoding=utf-8
syntax enable
set ai
set shiftwidth=2
set tabstop=2
set backspace=2
"set expandtab
set ruler
set number
set scrolloff=5                 " keep at least 5 lines above/below
set ttyfast                     " we have a fast terminal
set noerrorbells                " no beeps please
set visualbell
set shell=bash
set backup
set backupdir=~/.vim_backup     " keep backups from proliferating
set ff=unix
filetype plugin indent on

" settings for color scheme
" use 'set termguicolors' for terminals that support it
" 'set t_Co=256' for 256 terminal colors when TERM can't be defined
if &term =~ '256color'
	if has('termguicolors')
  	let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
  	let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
  	set termguicolors
	endif
	set background=dark
	colorscheme iceberg
	let g:airline_theme = 'iceberg'
endif
"set t_Co=256

:imap kj <Esc>
nmap <Leader>n :NERDTreeToggle<cr>
au VimEnter * RainbowParenthesesToggle
au Syntax * RainbowParenthesesLoadRound
au Syntax * RainbowParenthesesLoadSquare
au Syntax * RainbowParenthesesLoadBraces

function! CustomPreviousTab()
 if tabpagenr('$') == 1
  bp
 else
  tabprevious
 endif
endfunction
function! CustomNextTab()
 if tabpagenr('$') == 1
  bn
 else
  tabnext
 endif
endfunction
function! CustomNewTab()
 if tabpagenr('$') == 1
  new
 else
  tabnew
 endif
endfunction
nmap <silent> <Esc>k :call CustomPreviousTab()<cr>
nmap <silent> <Esc>j :call CustomNextTab()<cr>
nmap <silent> <Esc>c :if len(getwininfo()) == 1<cr>bd!<cr>else<cr>q!<cr>endif<cr>
nmap <silent> <Esc>n :call CustomNewTab()<cr>

" Remove trailing whitespace on save
au FileType rs,ruby,eruby,erlang,golang,go,java autocmd BufWritePre * :%s/\s\+$//e

" rust settings
let g:rustfmt_autosave = 1
let g:rustfmt_emit_files = 1
let g:rustfmt_fail_silently = 0

" Conquer of Completion
" Use tab for trigger completion with characters ahead and navigate
" NOTE: There's always complete item selected by default, you may want to enable
" no select by `"suggest.noselect": true` in your configuration file
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config
inoremap <silent><expr> <TAB>
    \ coc#pum#visible() ? coc#pum#next(1) :
    \ CheckBackspace() ? "\<Tab>" :
    \ coc#refresh()
inoremap <expr><S-TAB> coc#pum#visible() ? coc#pum#prev(1) : "\<C-h>"

function! CheckBackspace() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

if has('nvim')
  inoremap <silent><expr> <c-space> coc#refresh()
else
  inoremap <silent><expr> <c-@> coc#refresh()
endif

nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" set javascript ale fixer
let g:ale_fixers = {
\  'javascript': ['eslint'],
\  'typescript': ['eslint'],
\}
